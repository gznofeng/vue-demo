# vue-demo

> A Vue.js project

### Application Structure

```bash
dir
├── dist
└── src
    ├── assets
    ├── components
    ├── router
    ├── vuex
    └── main.js
```

### Start

```bash
    npm install

    npm run dev
    // develop mode

    npm run build
    // production mode
```