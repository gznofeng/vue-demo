var routes = [
    {
        path: '/',
        redirect: { name: 'index' }
    },{
        path: '/index',
        name: 'index',
        component: resolve => {
            require.ensure(['../views/index.vue'], () => {
                resolve(require('../views/index.vue'))
            })
        }
    },{
        path: '/user',
        name: 'user',
        component: resolve => {
            require.ensure(['../views/user.vue'], () => {
                resolve(require('../views/user.vue'))
            })
        }
    },{
        path: '/log_report',
        name: 'log_report',
        component: resolve => {
            require.ensure(['../views/log_report.vue'], () => {
                resolve(require('../views/log_report.vue'))
            })
        }
    }
]

export default routes;