import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './vuex'

import ElementUI from 'element-ui'

Vue.use(ElementUI)
import 'element-ui/lib/theme-default/index.css'


import VueResource from 'vue-resource';
Vue.use(VueResource);

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})
